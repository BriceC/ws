##### Go into the directory
cd ws

##### Build the project
docker build -t bcanet/ws .

##### Run it
docker run -p1234:8080 -d bcanet/ws

##### Check that the project is up and responding
curl "http://$(docker-machine ip default):1234/wsTest/"