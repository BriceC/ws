FROM tomcat
ADD wsTest.war /usr/local/tomcat/webapps/

CMD ["catalina.sh", "run"]
